import request from '@/utils/request';

const BaseUrl = window.g.API_HOST;

// 觸發器列表
export function getGameList() {
    return request({
        url: BaseUrl + 'getGameList',
        method: 'get',
    });
}

export function addGame(name) {
    return request({
        url: BaseUrl + 'game/',
        method: 'post',
        params: {
            gameName: name
        },
    });
}

export function updateGame(id, name) {
    return request({
        url: BaseUrl + 'game/',
        method: 'put',
        params: {
            gameId: id,
            gameName: name
        },
    });
}

export function deleteGame(ids) {
    return request({
        url: BaseUrl + 'game/',
        method: 'delete',
        params: {
            gameIds: ids,
        },
    });
}
