import request from '@/utils/request';

const BaseUrl = window.g.API_HOST;

// 觸發器列表
export function getGameDataList(search = '') {
    return request({
        url: BaseUrl + 'getGameDataList',
        method: 'get',
        params: {
            search: search
        },
    });
}

export function addGameData(data) {
    return request({
        url: BaseUrl + 'gameData/',
        method: 'post',
        data
    });
}

// export function updateGame(id, name) {
//     return request({
//         url: BaseUrl + 'gameData/',
//         method: 'put',
//         params: {
//             gameId: id,
//             gameName: name
//         },
//     });
// }

export function deleteGameData(ids) {
    return request({
        url: BaseUrl + 'gameData/',
        method: 'delete',
        params: {
            roundIds: ids,
        },
    });
}
