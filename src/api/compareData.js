import request from '@/utils/request';

const BaseUrl = window.g.API_HOST;

// 觸發器列表
export function getCompareDataList(gameId, playerId) {
    return request({
        url: BaseUrl + 'getCompareDataList',
        method: 'get',
        params: {
            gameId: gameId,
            playerId: playerId,
        },
    });
}

