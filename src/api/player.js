import request from '@/utils/request';

const BaseUrl = window.g.API_HOST;

// 觸發器列表
export function getPlayerList(search = '') {
    return request({
        url: BaseUrl + 'getPlayerList',
        method: 'get',
        params: {
            search: search
        },
    });
}

export function addPlayer(name) {
    return request({
        url: BaseUrl + 'player/',
        method: 'post',
        params: {
            playerName: name
        },
    });
}

export function updatePlayer(id, name) {
    return request({
        url: BaseUrl + 'player/',
        method: 'put',
        params: {
            playerId: id,
            playerName: name
        },
    });
}

export function deletePlayer(ids) {
    return request({
        url: BaseUrl + 'player/',
        method: 'delete',
        params: {
            playerIds: ids,
        },
    });
}
