import { Message } from 'element-ui';

export default class com {
    static dateFormat(v, format) {
        if(!v) return '';
        let d = v;
        // let format = 'yyyy-MM-dd';
        if(format === undefined) {
            format = 'yyyy-MM-dd hh:mm:ss';
        }
        if(typeof v === 'string') {
            if(v.indexOf('/Date(') > -1) {
                d = new Date(parseInt(v.replace('/Date(', '').replace(')/', ''), 10));
            } else {
                d = new Date(Date.parse(v.replace(/-/g, '/').replace('T', ' ').split('.')[0]));
            }// .split(".")[0] 用来处理出现毫秒的情况，截取掉.xxx，否则会出错
        }
        const o = {
            'M+': d.getMonth() + 1, // month
            'd+': d.getDate(), // day
            'h+': d.getHours(), // hour
            'm+': d.getMinutes(), // minute
            's+': d.getSeconds(), // second
            'q+': Math.floor((d.getMonth() + 3) / 3), // quarter
            'S': d.getMilliseconds(), // millisecond
        };
        if(/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (d.getFullYear() + '').substr(4 - RegExp.$1.length));
        }
        for(const k in o) {
            if(new RegExp('(' + k + ')').test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));
            }
        }
        return format;
    }

    static alertMsg(msg, type, duration = 3000) {
        let _type = 'info';
        switch(type) {
            case 0:
                _type = `success`;
                break;
            case 1:
                _type = `warning`;
                break;
            case 2:
                _type = `error`;
                break;
        }
        // alert(_msg);
        Message({
            message: msg,
            type: _type,
            duration: duration,
        });
    }

    static bootstrapTableOption = {
        method: 'get', // 请求方式（*）
        cache: false, // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true, // 是否显示分页（*）
        sortable: false, // 是否启用排序
        sortOrder: 'asc', // 排序方式
        cardView: false,
        sidePagination: 'client', // 分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, // 初始化加载第一页，默认第一页
        pageSize: 20, // 每页的记录行数（*）
        smartDisplay: false,
        pageList: [20, 40, 60, 80, 100],
        uniqueId: 'id', // 每一行的唯一标识，一般为主键列
        class: 'table text-nowrap table-condensed table-advanced cf table-hover',
        queryParamsType: '', // 預設值為 'limit' ,在預設情況下 傳給服務端的引數為：offset,limit,sort // 設定為 ''  在這種情況下傳給伺服器的引數為：pageSize,pageNumber
        queryParams: (params) => {
            return {
                page: params.pageNumber,
                limit: params.pageSize
            };
        },
        onLoadError: (o) => {
        },
        // 自定义加載訊息
        // formatLoadingMessage: function() {
        //     return '<span style="font-size: 12px">正在努力地加载数据中,请稍候</span>';
        // },
        // // 自定义分页字符串显示为中文
        formatShowingRows: function(pageFrom, pageTo, totalRows) {
            return `<span style="font-size: 12px">第 ${pageFrom} 到第 ${pageTo} 條，共 ${totalRows}筆 </span>`;
        },
        // // 自定义分页字符串显示为中文
        formatRecordsPerPage: function(pageNumber) {
            return ``;
        },
        formatNoMatches: function() {
            return '查不到資料';
        },
        // formatColumnsToggleAll: function() {
        //     return '切换所有';
        // }
    };
}
