import axios from 'axios';
import { Message } from 'element-ui';
import store from '@/store';
// import { getToken } from '@/utils/auth';

// create an axios instance
const service = axios.create({
    baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
    // withCredentials: true, // send cookies when cross-domain requests
    timeout: 5000 // request timeout
});

// request interceptor
service.interceptors.request.use(
    config => {
    // do something before request is sent

        if(store.getters.token) {
            // let each request carry token
            // ['X-Token'] is a custom headers key
            // please modify it according to the actual situation
            // config.headers['X-Token'] = getToken();
        }
        return config;
    },
    error => {
    // do something with request error
        console.log(error); // for debug
        return Promise.reject(error);
    }
);

// response interceptor
service.interceptors.response.use(
    /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

    /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
    response => {
        const res = response.data;

        switch(response.status) {
            case 200:
            case 204:
                return res;
            case 202:
                return null;
            default:
                // alert(`错误 : ${response.status}`);
                return res;
        }
    },
    error => {
        console.log('err' + error); // for debug
        const strs = error.message.split('Request failed with status code ');
        const msg = '';
        if(msg !== '') {
            Message({
                message: msg,
                type: 'error',
                duration: 3 * 1000
            });
        }
        return Promise.reject(strs[1]);
    }
);

export default service;
