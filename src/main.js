import Vue from 'vue';

import 'normalize.css/normalize.css'; // a modern alternative to CSS resets

import ElementUI from 'element-ui';
import './styles/element-variables.scss';
import locale from 'element-ui/lib/locale/lang/zh-TW'; // lang i18n

import '@/styles/index.scss'; // global css
import '@/styles/main.css'; // global css

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-table';
import '@/styles/bootstrap-table.css'; // global css

import App from './App';
import store from './store';
import router from './router';

import './icons'; // icon
import './permission'; // permission control
import './utils/error-log'; // error log
import com from '@/common/common';

import * as filters from './filters'; // global filters

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
if(process.env.NODE_ENV === 'production') {
    const { mockXHR } = require('../mock');
    mockXHR();
}

Vue.use(ElementUI, { locale });

// register global utility filters
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key]);
});

Vue.config.productionTip = false;
Vue.prototype.$com = com;

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
